// Schema Validation for Mongo collections, nice
SimpleSchema.debug = true;

var Schema = {};

Schema.UserCountry = new SimpleSchema({
    name: {
        type: String,
        optional: true
    },
    code: {
        type:  String,
        regEx: /^[A-Z]{2}$/,
        optional: true
    }
});

Schema.UserProfile = new SimpleSchema({
    firstName:    {
        type:     String,
        regEx:    /^[a-zA-Z-]{2,25}$/,
        optional: true
    },
    lastName:     {
        type:     String,
        regEx:    /^[a-zA-Z]{2,25}$/,
        optional: true
    },
    birthday:     {
        type:     Date,
        optional: true
    },
    gender:       {
        type:          String,
        allowedValues: [
            'Male',
            'Female'
        ],
        optional:      true
    },
    organization: {
        type:     String,
        regEx:    /^[a-z0-9A-z .]{3,30}$/,
        optional: true
    },
    website:      {
        type:     String,
        regEx:    SimpleSchema.RegEx.Url,
        optional: true
    },
    bio:          {
        type:     String,
        optional: true
    },
    country:      {
        type:     Schema.UserCountry,
        optional: true
    }
});

Schema.User = new SimpleSchema({
    username:            {
        type:  String,
        regEx: /^[a-z0-9A-Z_]{3,15}$/,
        optional: true

    },
    emails:              {
        type:     [Object], // this must be optional if you also use other login services like facebook,
        // but if you use only accounts-password, then it can be required
        optional: true
    },
    "emails.$.address":  {
        type:  String,
        regEx: SimpleSchema.RegEx.Email,
        optional: true
    },
    "emails.$.verified": {
        type: Boolean,
        optional: true
    },
    createdAt:           {
        type: Date,
        optional: true
    },
    profile:             {
        type:     Schema.UserProfile,
        optional: true
    },
    services:            {
        type:     Object,
        optional: true,
        blackbox: true
    }, // Add `roles` to your schema if you use the meteor-roles package.
    // Option 1: Object type
    // If you specify that type as Object, you must also specify the
    // `Roles.GLOBAL_GROUP` group whenever you add a user to a role.
    // Example:
    // Roles.addUsersToRoles(userId, ["admin"], Roles.GLOBAL_GROUP);
    // You can't mix and match adding with and without a group since
    // you will fail validation in some cases.
    //roles:               {
    //    type:     Object,
    //    optional: true,
    //    blackbox: true
    //}, // Option 2: [String] type
    // If you are sure you will never need to use role groups, then
    // you can specify [String] as the type
    //roles:               {
    //    type:     [String],
    //    optional: true
    //}
});

Schema.OnboardingMeta = new SimpleSchema({
    createdAt: {
        type:      Date,
        label:     "Time this list was created",
        autoValue: function() {
            if (this.isInsert) {
                return new Date;
            } else if (this.isUpsert) {
                return {$setOnInsert: new Date};
            } else {
                this.unset();  // Prevent user from supplying their own value
            }
        },
        denyUpdate: true,
        optional: true
    },
    updatedAt: {
        type:       Date,
        label:      "Last modified",
        autoValue:  function() {
            if (this.isUpdate) {
                return new Date();
            }
        },
        denyInsert: true,
        optional:   true
    },
    createdBy: {
        type:      String,
        label:     "Id of creator",
        optional: true,
        autoValue: function() {
            var cId = this.userId || 'system';
            if (this.isInsert) {

                return cId;
            } else if (this.isUpsert) {
                return {$setOnInsert: cId}
            } else {
                this.unset();
            }
        },
        // denyUpdate: true,
    },
    updatedBy: {
        type:     String,
        label:    "Last to modify",
        autoValue: function() {
            if (this.isUpdate) {
                return this.userId;
            }
        },
        denyInsert: true,
        optional: true
    },
    ownedBy:   {
        type:     String,
        label:    "Id of owner",
        autoValue: function() {
            var cId = this.userId || 'system';
            if (this.isInsert) {
                return cId;
            } else if (this.isUpsert) {
                return {$setOnInsert: cId}
            } else {
                this.unset();
            }
        },
        optional: true
    },
    isPrivate: {
        type:      Boolean,
        optional:  true,
        autoValue: function() {

            if (!this.operator) { // insert
                // default false
                if (!this.value) {
                    return false;
                }
            } else if (this.isSet) {
                // must be owner
                if (this.userId === this.field('ownedBy').value) {
                    return this.value;
                }
            }
            this.unset();
        }
    }
});

Schema.OnboardingNote = new SimpleSchema({
    meta:    {
        type:  Schema.OnboardingMeta,
        optional: true
    },
    content: {
        type:  String,
        label: "Note",
        optional: true
    }
});

Schema.baseTask = new SimpleSchema({
    meta:     {
        type:  Schema.OnboardingMeta,
        optional: true
    },
    name:     {
        type:   String,
        label:  "Task Name",
        // index:  -1,
        // sparse: true,
        max: 512,
        optional: true
    },
    isComplete: {
        type: Boolean,
        autoValue: function() {
            if (!this.operator && !this.isSet) {
                return false;
            }
        },
        optional: true
    },
    notes:    {
        type: [Schema.OnboardingNote],
        optional: true,
        max: 200
    }
});

Schema.OnboardingTask = new SimpleSchema([Schema.baseTask, {
    tasks: {
        type: [Schema.baseTask],
        optional: true,
        max: 50
    }
}]);

Schema.OnboardingList = new SimpleSchema({
    meta:            {
        type:  Schema.OnboardingMeta,
        optional: true
    },
    title:           {
        type:  String,
        label: "Title",
        max:   128,
        optional: true
    },
    clientDomain:    {
        type:  String,
        label: "Client's Captina site domain",
        regEx: SimpleSchema.RegEx.Domain,
        optional: true
    },
    incompleteCount: {
        type:  Number,
        label: "Remaining Tasks",
        min:   0,
        optional: true
    },
    dueDate:         {
        type:     Date,
        label:    "Due date",
        optional: true
    },
    tasks:           {
        type: [Schema.OnboardingTask],
        optional: true,
        max: 200
    },
    notes:           {
        type: [Schema.OnboardingNote],
        optional: true,
        max: 200
    }
});

Meteor.users.attachSchema(Schema.User);

// Calculate a default name for a list in the form of 'List A'
//Lists.defaultName = function() {
//    var nextLetter = 'A', nextName = 'List ' + nextLetter;
//    while (Lists.findOne({name: nextName})) {
//        // not going to be too smart here, can go past Z
//        nextLetter = String.fromCharCode(nextLetter.charCodeAt(0) + 1);
//        nextName   = 'List ' + nextLetter;
//    }
//
//    return nextName;
//};

Lists = new Mongo.Collection('lists');
Lists.attachSchema(Schema.OnboardingList);
