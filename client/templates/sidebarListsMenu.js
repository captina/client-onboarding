Template.sidebarListsMenu.helpers({
   //add you helpers here
   activeListClass: function() {
      var currentListId = FlowRouter.getParam('listId');
      if (FlowRouter.getRouteName() == 'showList' && currentListId == this._id) {
         return 'active';
      }

   }
});

Template.sidebarListsMenu.events({
   //add your events here
   //'mouseover #toc.sidebar.menu.left .list-title.item': function(event, template) {
   //   template.$(event.target)
   //      .show();
   //},
   'ciick .mtr-add-list': function() {
      var goofyStrings = [
             'goober',
             'Doofus Rainier',
             'walls',
             'Bridges',
             'creepers.com'
          ],
          goofyTitle = [
             Random.hexString(5) + ':',
             Random.choice(goofyStrings) + ',',
             Random.hexString(8),
             Random.choice(goofyStrings)
          ];


      var listTitle = goofyTitle.join(' ');
      var newList = Lists.insert({title: goofyTitle, incompleteCount: 0});
      FlowRouter.go(FlowRouter.path('showList', {listId: newList._id}));
   }
});

Template.sidebarListsMenu.onCreated(function() {
   //add your statement here
});

Template.sidebarListsMenu.onRendered(function() {
   //add your statement here
   // list notes popup
   var listId = Template.currentData()._id;
   this.$('#sb-' + listId)
      .popup({
         inline:    false,
         hoverable: false,
         movePopup: true,
         setFluidWidth: false,
         position:  'top center',
         delay:     {
            show: 50,
            hide: 10
         },
         name: '#sb-' + listId,
         debug: false,
         verbose: false
         //popup: '#note-' + listId
      });
});

Template.sidebarListsMenu.onDestroyed(function() {
   //add your statement here
});

