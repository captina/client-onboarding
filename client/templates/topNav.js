Template.topNav.helpers({
   //add you helpers here
   avatarUrl: function() {
      var email = 'david@captina.net', hash;
      // https://en.gravatar.com/site/implement/images/
      return 'https://secure.gravatar.com/avatar/8a1f1a36cb59fe985006b869769d1623.png?size=24';
   },
   emailNamePart: function() {
      var email = Meteor.user().emails[0].address;
      return email.substring(0, email.indexOf('@'));
   }
});

//Template.topNav.events({
//   //add your events here
//});
//
//Template.topNav.onCreated(function() {
//   //add your statement here
//});
//
//Template.topNav.onRendered(function() {
//   //add your statement here
//});
//
//Template.topNav.onDestroyed(function() {
//   //add your statement here
//});

