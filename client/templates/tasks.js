
Template.tasks.onCreated(function() {

   // highlight.js
   var template = this;
   this.autorun(function() {
      template.subscribe('mainTasks', FlowRouter.getParam('listId'));
   });
});

//Template.tasks.onRendered(function() {
//   this.autorun(function() {
//      if (!this.subscription.ready()) {
//         this.$('.loading')
//            .removeClass('hidden')
//            .addClass('active');
//      }
//      else {
//      }
//   }
//      .bind(this));
//});

Template.tasks.helpers({
   mainTasks: function() {
      var cursor = Lists.find({ _id: FlowRouter.getParam('listId') }, {fields: { tasks: 1 }});
      console.log(EJSON.stringify(cursor.fetch(), true));
      return cursor;
   }
});

//Template.tasks.events({
//   //add your events here
//});
//
//Template.tasks.onCreated(function() {
//   //add your statement here
//});
//
//Template.tasks.onRendered(function() {
//   //add your statement here
//});
//
//Template.tasks.onDestroyed(function() {
//   //add your statement here
//});
//
