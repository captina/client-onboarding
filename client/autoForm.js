/**
 * Project: Client-Onboard
 * Package:
 * Module: autoForm.js
 * Author: lents
 * Date: 9/23/15
 * Create with: IntelliJ IDEA.
 */


Meteor.startup(function() {
   // required for pre-built css, expecting #toc, etc to be child of <body>
   BlazeLayout.setRoot('body');

   // attach ready event
   AutoForm.setDefaultTemplate("semanticUI");
});