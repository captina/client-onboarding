/**
 * Template: GridLayout
 */

Template.GridLayout.helpers({
    avatarUrl: function () {
        var email = 'david@captina.net'; // fetch
        // https://en.gravatar.com/site/implement/images/
        return 'https://secure.gravatar.com/avatar/8a1f1a36cb59fe985006b869769d1623';
    }
});