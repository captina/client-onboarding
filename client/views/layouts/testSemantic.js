/**
 * Template: GridLayout
 */

Template.testSemantic.onCreated(function() {
   // highlight.js
   var template = this;
   template.subscribe('lists', function() {
      // Wait for the data to load using the callback
      Tracker.afterFlush(function() {
         // Use Tracker.afterFlush to wait for the UI to re-render
         // then use highlight.js to highlight a code snippet
         var $code = template.find('code');
         if ($code) {
            hljs.highlightBlock($code);
         }
      });
   });
});

Template.testSemantic.onRendered(function() {
   var $listMenu = this.$('#toc');
   $listMenu.sidebar({
      dimPage:          false,
      transition:       'slide',
      mobileTransition: 'uncover'
   });
   $listMenu.sidebar('attach events', '.launch.button, .view-ui, .launch.item');
});

Template.testSemantic.helpers({
   lists: function() {
      return Lists.find({}, {
         fields: {
            title:           1,
            notes:           1,
            incompleteCount: 1
         }
      });
   }
});

//Template.testSemantic.events({
//
//
//});