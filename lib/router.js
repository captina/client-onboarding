//if (Meteor.isClient) {
//    // XXX: maybe to render login modal autmatically? see https://github.com/iron-meteor/iron-router/blob/devel/Guide.md#hooks
//    // Init Foundation
//    Router.onAfterAction(function() {
//        $(document).foundation(); // or single plugin
//    });
//}

AccountsTemplates.configure({
   defaultLayout:        'testSemantic',
   defaultLayoutRegions: {},
   defaultContentRegion: 'main'
});

AccountsTemplates.configureRoute('signIn', {
   path:           '/login',
   layoutTemplate: 'testSemantic',
   contentRegion:  'main'
});

//FlowRouter.subscriptions = function() {
//   this.register('allLists', Meteor.subscribe('lists'));
//};

FlowRouter.route('/lists', {
   action:        function() {
      BlazeLayout.render('testSemantic', {main: 'testContent'});
   }
});

FlowRouter.route('/list/:listId', {
   name: 'showList',
   action: function() {
      BlazeLayout.render('testSemantic', {main: 'tasks'});
   }
});