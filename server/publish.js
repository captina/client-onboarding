
//if (Meteor.userId) {
    Meteor.publish('lists', function() {
        return Lists.find({}, { fields: { title: 1, notes: 1, incompleteCount: 1} });
    });

   Meteor.publish('mainTasks', function(listId) {
      check(listId, String);
      var tasks = Lists.find({_id: listId}, {fields: { tasks: 1}});
      // console.log('publish tasks for %s: ', listId, EJSON.stringify(tasks.fetch()));
      return tasks;
   });

   //Meteor.publish('task', function(taskId) {
   //     check(taskId, String);
   //     return Lists.find({_id: taskId});
   // });
//}
