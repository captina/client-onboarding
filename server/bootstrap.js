// if the database is empty on server start, create some sample data.
Meteor.startup(function() {

    if (Lists.find().count() <= 10) {
        YamlConfig.loadFiles(Assets);

        Meteor.call('getClientConfig', function(err, result) {
            if (err) {
                console.log(err);
            } else {
                var data = result['templateLists'];
                console.log(EJSON.stringify(data, {indent: 3, canonical: true}));

                _.each(data, function(list) {
                    list.incompleteCount = list.tasks.length;
                    var listId           = Lists.insert(list);
                    console.log('insert _id: %s', listId);
                    console.log(EJSON.stringify(list, {indent: 3, canonical: true}));
                });
            }
        });
    }
});

