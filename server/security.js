/**
 * Project: Client-Onboard
 * Package:
 * Module: security
 * Author: lents
 * Date: 9/15/15
 * docs: https://atmospherejs.com/ongoworks/security
 * Created with: IntelliJ IDEA.
 */

    // Clients may insert posts only if a user is logged in
//Lists.permit(['insert', 'update', 'remove']).ifLoggedIn().apply();